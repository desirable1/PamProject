# HTML Web page for a SIMPLE routine in JAVASCRIPT

## The Purpose is thus:

* **_It accepts a name of any animal from a user._**

* **_Checks if the name given is in a list of animals._**

* **_Gives a response to the user, if the name exists or not._**

### Procedures:

1. Created a folder on my Personal Computer (with a suitable name - PamProject)

* launched Sublime text (A text editor) and opened the folder created above.
* Inside the Folder, I created HTML file and saved it as **index.html**.
* In the head section, I included the script element tag (<script> and </script>) which is a *javascript function.*
...However, I used an alert box to ensure that the user understands what the site is all about before entering.
* Then, within that script tags I declared a static list of animals using an **array**, a *data structure* in JavaScript.

2. Prompts the **User** to *guess* a name of an animal present in the list I created (It's actually tricky !)

* I achieved that using the in-built functions **window.prompt** and **window.alert** to interact with the **User**

3. Takes the value given by the user and checks if *Present* within the list of my animal array.

* I used the **If and Else Statement** to deploy this
* Then Responds to the user if the *Guess* is right or wrong using the **.includes**

#### Installations
* Sublime Text
* Git

#### Registerations
* Github
* Gitlab
* Heroku

## Benefits of this Project
* _A robust understanding of World wide web, data structures and algorithm_
* _The Fundamentals of HTML, CSS and JavaScript_
* _A good understanding of Version Control - GIT_ :
..* Creating Repository on Gitlab, Pulling from and Pushing to the repository
..* Contributing to an open source project via forking and Pull Request
* _Wonderful Grasp of Heroku, Deployment and Hosting_ then updating Online to GIT


#### To View the site and Explore Click [Here](https://my-awesome-site.herokuapp.com/)

DEVELOPED AND DESIGNED BY [Pamela Agbakoba](http://www.agbakobapamela.org)
*CEO*_BooustD_ nationwide.** &copy; 2018
